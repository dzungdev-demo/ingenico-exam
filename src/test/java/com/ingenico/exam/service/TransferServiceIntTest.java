package com.ingenico.exam.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ingenico.exam.AbstractIntegrationTest;
import com.ingenico.exam.constant.Constants;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.entity.Transfer;
import com.ingenico.exam.repository.AccountRepository;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferServiceIntTest.
 */
public class TransferServiceIntTest extends AbstractIntegrationTest{

	/** The transfer service. */
	@Autowired
	private TransferService transferService;

	/** The account repository. */
	@Autowired
	private AccountRepository accountRepository;

	/** The thread pool size. */
	private final int threadPoolSize = 15;

	/** The run times. */
	private final int runTimes = 502;

	/**
	 * Test given dzung balance 1 mil when transfer 502 times each 2000 then 2
	 * times will not run and balance is 0.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testGivenDzungBalance1Mil_whenTransfer502timesEach2000_then2TimesWillNotRunAndBalanceIs0()
			throws Exception {
		final ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);
		final List<Future<Void>> futures = new ArrayList<Future<Void>>();
		for (int x = 0; x < runTimes; x++) {
			final Callable<Void> callable = new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					final Transfer transfer = new Transfer(1L, 2L, 2000);
					TransferServiceIntTest.this.transferService.save(transfer);
					return null;
				}
			};
			final Future<Void> submit = executorService.submit(callable);
			futures.add(submit);
		}

		final List<Exception> exceptions = new ArrayList<Exception>();
		for (final Future<Void> future : futures) {
			try {
				future.get();
			} catch (final Exception e) {
				exceptions.add(e);
			}
		}

		final Account dzungAccount = accountRepository.findOne(Constants.DZUNG_ACCOUNT_ID);
		Assertions.assertThat(dzungAccount.getBalance()).isEqualTo(0);
		Assertions.assertThat(exceptions.size()).isEqualTo(2);

		executorService.shutdown();
	}

}
