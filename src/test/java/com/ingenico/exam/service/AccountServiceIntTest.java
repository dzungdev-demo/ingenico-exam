package com.ingenico.exam.service;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ingenico.exam.AbstractIntegrationTest;
import com.ingenico.exam.constant.Constants;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.exception.DuplicatedException;
import com.ingenico.exam.exception.NotFoundException;
import com.ingenico.exam.exception.NotPositiveAmountException;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class AccountServiceIntTest.
 */
public class AccountServiceIntTest extends AbstractIntegrationTest {

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/**
	 * Test create one new account when balance negative then over drawn
	 * exception.
	 *
	 * @throws DuplicatedException
	 *             the duplicated exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@Test(expected = NotPositiveAmountException.class)
	public void testCreateOneNewAccount_whenBalanceNegative_thenOverDrawnException()
			throws DuplicatedException, NotPositiveAmountException {
		final Account clarkeAccount = new Account("Clarke", -1);
		accountService.save(clarkeAccount);
	}

	/**
	 * Test create one new account when name is dzung then duplicated exception.
	 *
	 * @throws DuplicatedException
	 *             the duplicated exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@Test(expected = DuplicatedException.class)
	public void testCreateOneNewAccount_whenNameIsDzung_thenDuplicatedException()
			throws DuplicatedException, NotPositiveAmountException {
		final Account dzungAccount = new Account(Constants.DZUNG_ACCOUNT_NAME, 200);
		accountService.save(dzungAccount);
	}

	/**
	 * Test find all then return 2 account.
	 *
	 * @throws DuplicatedException
	 *             the duplicated exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@Test()
	public void testFindAll_thenReturn2Account() throws DuplicatedException, NotPositiveAmountException {
		final List<Account> allAccounts = accountService.findAll();
		Assertions.assertThat(allAccounts.size()).isEqualTo(2);
	}

	/**
	 * Test find by dzung id then return dzung account.
	 *
	 * @throws DuplicatedException
	 *             the duplicated exception
	 * @throws NotFoundException
	 *             the not found exception
	 */
	@Test()
	public void testFindByDzungId_thenReturnDzungAccount() throws DuplicatedException, NotFoundException {
		final Account dzungAccount = accountService.findByName(Constants.DZUNG_ACCOUNT_NAME);
		Assertions.assertThat(dzungAccount.getId()).isEqualTo(1);
	}

	/**
	 * Test create one new account when balance positive then there are 3
	 * accounts.
	 *
	 * @throws DuplicatedException
	 *             the duplicated exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@Test
	public void testCreateOneNewAccount_whenBalancePositive_thenThereAre3Accounts()
			throws DuplicatedException, NotPositiveAmountException {
		Assertions.assertThat(accountService.findAll().size()).isEqualTo(2);

		final Account kenAccount = new Account("Ken", 200);
		accountService.save(kenAccount);

		Assertions.assertThat(accountService.findAll().size()).isEqualTo(3);
	}
}
