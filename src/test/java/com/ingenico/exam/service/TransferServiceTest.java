package com.ingenico.exam.service;

import static org.mockito.Mockito.when;

import java.util.Locale;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.test.util.ReflectionTestUtils;

import com.ingenico.exam.constant.Constants;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.entity.Transfer;
import com.ingenico.exam.exception.NotPositiveAmountException;
import com.ingenico.exam.exception.OverDrawnException;
import com.ingenico.exam.repository.AccountRepository;
import com.ingenico.exam.repository.TransferRepository;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferServiceTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

	/** The transfer repository. */
	@Mock
	private TransferRepository transferRepository;

	/** The account repository. */
	@Mock
	private AccountRepository accountRepository;

	/** The message source. */
	@Mock
	private MessageSource messageSource;

	/** The transfer service. */
	@Spy
	private TransferService transferService;

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		ReflectionTestUtils.setField(transferService, "transferRepository", transferRepository);
		ReflectionTestUtils.setField(transferService, "accountRepository", accountRepository);
		ReflectionTestUtils.setField(transferService, "messageSource", messageSource);
	}

	/**
	 * Test transfer from dzung to hong then successfully.
	 *
	 * @throws OverDrawnException
	 *             the over drawn exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@Test
	public void testTransferFromDzungToHong_thenSuccessfully() throws OverDrawnException, NotPositiveAmountException {

		final Account dzungAccount = new Account("Dzung", 200);
		final Account hongAccount = new Account("Hong", 150);

		final Transfer transfer = new Transfer(1L, 2L, 50);
		transfer.setFromAccountId(1L);
		transfer.setToAccountId(2L);

		when(accountRepository.findById(1L)).thenReturn(dzungAccount);
		when(accountRepository.findById(2L)).thenReturn(hongAccount);
		when(transferRepository.save(transfer)).thenReturn(transfer);

		transferService.save(transfer);

		Assertions.assertThat(dzungAccount.getBalance()).isEqualTo(150);
		Assertions.assertThat(hongAccount.getBalance()).isEqualTo(200);

	}

	/**
	 * Test transfer amount bigger than dzung balance then over drawn exception.
	 *
	 * @throws OverDrawnException
	 *             the over drawn exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@Test(expected = OverDrawnException.class)
	public void testTransferAmountBiggerThanDzungBalance_thenOverDrawnException()
			throws OverDrawnException, NotPositiveAmountException {
		final Account dzungAccount = new Account("Dzung", 200);
		final Account hongAccount = new Account("Hong", 150);

		final Transfer transfer = new Transfer(Constants.DZUNG_ACCOUNT_ID, Constants.HONG_ACCOUNT_ID, 201);
		transfer.setFromAccountId(Constants.DZUNG_ACCOUNT_ID);
		transfer.setToAccountId(Constants.HONG_ACCOUNT_ID);

		when(accountRepository.findById(Constants.DZUNG_ACCOUNT_ID)).thenReturn(dzungAccount);
		when(accountRepository.findById(Constants.HONG_ACCOUNT_ID)).thenReturn(hongAccount);
		when(messageSource.getMessage("", null, Locale.US)).thenReturn("");
		when(transferRepository.save(transfer)).thenReturn(transfer);

		transferService.save(transfer);
	}

	/**
	 * Test transfer negative amount then not positive amount exception.
	 *
	 * @throws OverDrawnException
	 *             the over drawn exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@Test(expected = NotPositiveAmountException.class)
	public void testTransferNegativeAmount_thenNotPositiveAmountException()
			throws OverDrawnException, NotPositiveAmountException {
		final Account dzungAccount = new Account("Dzung", 200);
		final Account hongAccount = new Account("Hong", 150);

		final Transfer transfer = new Transfer(Constants.DZUNG_ACCOUNT_ID, Constants.HONG_ACCOUNT_ID, -10);
		transfer.setFromAccountId(Constants.DZUNG_ACCOUNT_ID);
		transfer.setToAccountId(Constants.HONG_ACCOUNT_ID);

		when(accountRepository.findById(Constants.DZUNG_ACCOUNT_ID)).thenReturn(dzungAccount);
		when(accountRepository.findById(Constants.HONG_ACCOUNT_ID)).thenReturn(hongAccount);
		when(messageSource.getMessage("", null, Locale.US)).thenReturn("");
		when(transferRepository.save(transfer)).thenReturn(transfer);

		transferService.save(transfer);
	}

}
