package com.ingenico.exam;

import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author DUNG TAN DANG
 * 
 * 		   The Class AbstractIntegrationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
@IntegrationTest({ "server.port=0", "management.port=0" })
@Transactional
@TestPropertySource(locations = { "classpath:test-application.properties" })
public abstract class AbstractIntegrationTest {

}
