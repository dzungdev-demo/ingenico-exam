package com.ingenico.exam.api.soap;

import java.io.IOException;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.ws.test.server.RequestCreators;
import org.springframework.xml.transform.StringSource;

import com.ingenico.exam.App;
import com.ingenico.exam.SoapConfig;
import com.ingenico.exam.exception.NotFoundException;
import com.ingenico.exam.service.AccountService;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class AccountEndpointIntTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { App.class, SoapConfig.class })
@WebAppConfiguration
@TestPropertySource(locations = { "classpath:test-application.properties" })
public class AccountEndpointIntTest {

	/** The application context. */
	@Autowired
	private ApplicationContext applicationContext;

	/** The mock client. */
	private MockWebServiceClient mockClient;

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {

		Assert.assertNotNull(applicationContext);
		mockClient = MockWebServiceClient.createClient(applicationContext);
	}

	/**
	 * Test create john account then successfully.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws NotFoundException
	 *             the not found exception
	 */
	@Test
	public void testCreateJohnAccount_thenSuccessfully() throws IOException, NotFoundException {
		final String request = 	"<payment:AccountRequest xmlns:payment=\"http://ingenico.com/exam/api/soap/schemas\">"
								+ 	"<payment:Account>" 
								+ 		"<payment:Name>John</payment:Name>" 
								+ 		"<payment:Balance>200</payment:Balance>"
								+ 	"</payment:Account>" 
								+"</payment:AccountRequest>";

		mockClient.sendRequest(RequestCreators.withPayload(new StringSource(request)));

		// final Account johnAccount = accountService.findAll().size();
		Assertions.assertThat(accountService.findAll().size()).isEqualTo(3);

		accountService.delete(3L);

	}

}
