package com.ingenico.exam.api.soap;

import java.io.IOException;
import java.util.Arrays;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.ws.test.server.RequestCreators;
import org.springframework.xml.transform.StringSource;

import com.ingenico.exam.App;
import com.ingenico.exam.SoapConfig;
import com.ingenico.exam.constant.Constants;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.exception.DuplicatedException;
import com.ingenico.exam.exception.NotFoundException;
import com.ingenico.exam.service.AccountService;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferEndpointIntTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { App.class, SoapConfig.class })
@WebAppConfiguration
@TestPropertySource(locations = { "classpath:test-application.properties" })
public class TransferEndpointIntTest {

	/** The application context. */
	@Autowired
	private ApplicationContext applicationContext;

	/** The mock client. */
	private MockWebServiceClient mockClient;

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {

		Assert.assertNotNull(applicationContext);
		mockClient = MockWebServiceClient.createClient(applicationContext);
	}

	/**
	 * Test sending holiday request.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DuplicatedException 
	 * @throws NotFoundException 
	 */
	@Test
	public void testSendingHolidayRequest() throws IOException, NotFoundException, DuplicatedException {
		final String request = "<payment:TransferRequest xmlns:payment=\"http://ingenico.com/exam/api/soap/schemas\">"
								+ " <payment:Transfer>" 
								+ 		" <payment:FromAccountId>1</payment:FromAccountId>"
								+ 		" <payment:ToAccountId>2</payment:ToAccountId>" 
								+ 		" <payment:Amount>50</payment:Amount>"
								+ 	"</payment:Transfer>" 
								+"</payment:TransferRequest>";

		mockClient.sendRequest(RequestCreators.withPayload(new StringSource(request)));

		final Account hongAccount = accountService.findOne(Constants.HONG_ACCOUNT_ID);
		Assertions.assertThat(hongAccount.getBalance()).isEqualTo(150);
		
		// update data as back to before test
		final Account dzungAccount = accountService.findOne(Constants.DZUNG_ACCOUNT_ID);
		dzungAccount.setBalance(1_000_000);
		hongAccount.setBalance(100);
		accountService.save(Arrays.asList(dzungAccount, hongAccount));

	}
}
