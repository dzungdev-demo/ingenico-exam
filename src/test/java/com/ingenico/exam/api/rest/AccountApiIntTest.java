package com.ingenico.exam.api.rest;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ingenico.exam.AbstractIntegrationTest;
import com.ingenico.exam.constant.Constants;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.service.AccountService;
import com.ingenico.exam.util.TestUtil;

/**
 * The Class AccountApiIntTest.
 *
 * @author DUNG TAN DANG
 * 
 *         The Class AccountApiIntTest.
 */

public class AccountApiIntTest extends AbstractIntegrationTest{

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/** The rest account api mock mvc. */
	private MockMvc restAccountApiMockMvc;

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		final AccountApi accountApi = new AccountApi();
		ReflectionTestUtils.setField(accountApi, "accountService", accountService);

		restAccountApiMockMvc = MockMvcBuilders.standaloneSetup(accountApi).build();
	}

	/**
	 * Test create new clarke account when name is unique balance positive then
	 * HTTP 201 return.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testCreateNewClarkeAccount_whenNameIsUniqueBalancePositive_thenHTTP201Return()
			throws IOException, Exception {
		final Account clarkeAccount = new Account("Clarke", 200);

		restAccountApiMockMvc.perform(post(Constants.ACCOUNTS_API_BASE_PATH)
				.content(TestUtil.convertObjectToJsonBytes(clarkeAccount)).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isCreated());

	}
	
	/**
	 * Test find all then return hong and dzung account (there are only two account in database
	 * test-data.sql file)
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFindAll_thenReturnHongAndDzungAccount() throws Exception {
		restAccountApiMockMvc.perform(get(Constants.ACCOUNTS_API_BASE_PATH)
							 .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
							 .andExpect(status().isOk())
							 .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
							 .andExpect(jsonPath("$", hasSize(2)))
							 .andExpect(jsonPath("$[0].name").value(Constants.DZUNG_ACCOUNT_NAME))
							 .andExpect(jsonPath("$[1].name").value(Constants.HONG_ACCOUNT_NAME));
	}
	
	/**
	 * Test given dzung account id 1 when find by id 1 then return dzung account.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGivenDzungAccountId1_whenFindById1_thenReturnDzungAccount() throws Exception {
		final String url = Constants.ACCOUNTS_API_BASE_PATH + "/" + Constants.DZUNG_ACCOUNT_ID;
		restAccountApiMockMvc.perform(get(url)
							 .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
							 .andExpect(status().isOk())
							 .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
							 .andExpect(jsonPath("$.name").value(Constants.DZUNG_ACCOUNT_NAME));
	}
	
	/**
	 * Test given hong account when update balance plus 50 then balance is 150.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws Exception the exception
	 */
	@Test
	public void testGivenHongAccount_whenUpdateBalancePlus50_thenBalanceIs150() throws IOException, Exception {
		final String url = Constants.ACCOUNTS_API_BASE_PATH + "/" + Constants.HONG_ACCOUNT_ID;
		final Account hongAccount = accountService.findOne(Constants.HONG_ACCOUNT_ID);
		hongAccount.setBalance(hongAccount.getBalance() + 50);
		
		restAccountApiMockMvc.perform(put(url)
							 .content(TestUtil.convertObjectToJsonBytes(hongAccount))
							 .contentType(MediaType.APPLICATION_JSON_UTF8))
							 .andExpect(status().isOk());
		
		final Account updateHongAccount = accountService.findOne(Constants.HONG_ACCOUNT_ID);
		Assertions.assertThat(updateHongAccount.getBalance()).isEqualTo(150);
	}

}
