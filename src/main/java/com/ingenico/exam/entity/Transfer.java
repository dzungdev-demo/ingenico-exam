package com.ingenico.exam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class Transfer.
 */
@Entity
@XmlType(namespace = "http://www.example.org/transfer")
public class Transfer extends AbstractEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4773661669380477596L;

	/** The from account. */
	@ManyToOne
	@JoinColumn(name = "from_account_id", referencedColumnName = "id")
	@JsonIgnore
	private Account fromAccount;

	/** The to account. */
	@ManyToOne
	@JoinColumn(name = "to_account_id", referencedColumnName = "id")
	@JsonIgnore
	private Account toAccount;

	/** The from account id. */
	@Column(name = "from_account_id", insertable = false, updatable = false)
	@NotNull
	private Long fromAccountId;

	/** The to account id. */
	@Column(name = "to_account_id", insertable = false, updatable = false)
	@NotNull
	private Long toAccountId;

	/** The amount. */
	@Column(name = "amount")
	@NotNull
	private double amount;

	/**
	 * Instantiates a new transfer.
	 */
	public Transfer() {
	}

	/**
	 * Instantiates a new transfer.
	 *
	 * @param fromAccount
	 *            the from account
	 * @param toAccount
	 *            the to account
	 * @param amount
	 *            the amount
	 */
	public Transfer(final Account fromAccount, final Account toAccount, final double amount) {
		super();
		this.fromAccount = fromAccount;
		this.toAccount = toAccount;
		this.amount = amount;
	}

	/**
	 * Instantiates a new transfer.
	 *
	 * @param fromAccountId
	 *            the from account id
	 * @param toAccountId
	 *            the to account id
	 * @param amount
	 *            the amount
	 */
	public Transfer(final Long fromAccountId, final Long toAccountId, final double amount) {
		super();
		this.fromAccountId = fromAccountId;
		this.toAccountId = toAccountId;
		this.amount = amount;
	}

	/**
	 * Gets the from account.
	 *
	 * @return the from account
	 */
	public Account getFromAccount() {
		return fromAccount;
	}

	/**
	 * Sets the from account.
	 *
	 * @param fromAccount
	 *            the new from account
	 */
	public void setFromAccount(final Account fromAccount) {
		this.fromAccount = fromAccount;
		if (fromAccount != null)
			this.fromAccountId = fromAccount.getId();
	}

	/**
	 * Gets the to account.
	 *
	 * @return the to account
	 */
	public Account getToAccount() {
		return toAccount;
	}

	/**
	 * Sets the to account.
	 *
	 * @param toAccount
	 *            the new to account
	 */
	public void setToAccount(final Account toAccount) {
		this.toAccount = toAccount;
		if (toAccount != null)
			this.toAccountId = toAccount.getId();
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(final double amount) {
		this.amount = amount;
	}

	/**
	 * Gets the from account id.
	 *
	 * @return the from account id
	 */
	public Long getFromAccountId() {
		return fromAccountId;
	}

	/**
	 * Sets the from account id.
	 *
	 * @param fromAccountId
	 *            the new from account id
	 */
	public void setFromAccountId(final Long fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	/**
	 * Gets the to account id.
	 *
	 * @return the to account id
	 */
	public Long getToAccountId() {
		return toAccountId;
	}

	/**
	 * Sets the to account id.
	 *
	 * @param toAccountId
	 *            the new to account id
	 */
	public void setToAccountId(final Long toAccountId) {
		this.toAccountId = toAccountId;
	}

}
