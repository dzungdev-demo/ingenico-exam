package com.ingenico.exam.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.exam.api.rest.dto.ErrorDTO;
import com.ingenico.exam.entity.Transfer;
import com.ingenico.exam.exception.NotPositiveAmountException;
import com.ingenico.exam.exception.OverDrawnException;
import com.ingenico.exam.service.TransferService;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferApi.
 */
@RestController
@RequestMapping(value = "/api/v1/transfers")
public class TransferApi {

	/** The transfer service. */
	@Autowired
	private TransferService transferService;

	/**
	 * Creates the transfer.
	 *
	 * @param transfer
	 *            the transfer
	 * @return the response entity
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> createTransfer(@RequestBody final Transfer transfer)
			throws NotPositiveAmountException {

		try {
			transferService.save(transfer);
			return new ResponseEntity<Object>(HttpStatus.CREATED);
		} catch (final OverDrawnException e) {

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ErrorDTO(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()));
		} catch (final NotPositiveAmountException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ErrorDTO(e.getMessage(), HttpStatus.BAD_REQUEST.value()));
		}

	}

}
