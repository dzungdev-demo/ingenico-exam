package com.ingenico.exam.api.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.exam.api.rest.dto.ErrorDTO;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.exception.DuplicatedException;
import com.ingenico.exam.exception.NotFoundException;
import com.ingenico.exam.exception.NotPositiveAmountException;
import com.ingenico.exam.service.AccountService;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class AccountApi.
 */
@RestController
@RequestMapping(value = "/api/v1/accounts")
public class AccountApi {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AccountApi.class);

	/** The Account service. */
	@Autowired
	private AccountService accountService;

	/**
	 * Find all.
	 *
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Account>> findAll() {
		log.info("start findAll Account");

		final List<Account> accounts = accountService.findAll();
		if (accounts.isEmpty()) {
			log.info("end findAll Account NO_CONTENT:204");

			return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
		}
		log.info("end findAll Account OK:200");

		return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Account> findById(@PathVariable("id") final long id) {
		log.info("start findById with id is {}", id);

		final Account account = accountService.findOne(id);
		if (account == null) {
			return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
		}
		log.info("end findById with id is {}", id);
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}

	/**
	 * Creates a Account.
	 *
	 * @param account
	 *            the Account
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> create(@Validated @RequestBody final Account account) {
		log.info("start creating new Account with account name {}", account.getName());

		try {
			accountService.save(account);
			log.info("end creating new Account with account name {}", account.getName());

			return new ResponseEntity<Object>(HttpStatus.CREATED);
		} catch (final DuplicatedException de) {
			log.error("conflict creating new Account with account name {}", account.getName());

			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(new ErrorDTO(de.getMessage(), HttpStatus.CONFLICT.value()));
		} catch (final NotPositiveAmountException ode) {
			log.error("NotPositiveAmountException creating new Account with account name {}", account.getName());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ErrorDTO(ode.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()));
		}
	}

	/**
	 * Update Account.
	 *
	 * @param account
	 *            the Account
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Validated @RequestBody final Account account) {
		log.info("start update account with account name {}", account.getName());
		try {
			accountService.update(account);
			log.info("end update account with account name {}", account.getName());
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (final NotFoundException nfe) {
			log.error("Notfound: 404 in update account with account name {}", account.getName());

			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (final DuplicatedException de) {
			log.error("Notfound: 409 in update account with account name {}", account.getName());

			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

}
