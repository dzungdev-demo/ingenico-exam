package com.ingenico.exam.api.rest.dto;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class ErrorDTO.
 */
public class ErrorDTO {
	/** The message. */
	private String message;

	/** The http status code. */
	private int httpStatusCode;

	/**
	 * Instantiates a new error DTO.
	 *
	 * @param message
	 *            the message
	 * @param httpStatusCode
	 *            the http status code
	 */
	public ErrorDTO(final String message, final int httpStatusCode) {
		super();
		this.message = message;
		this.httpStatusCode = httpStatusCode;
	}

	/**
	 * Instantiates a new error DTO.
	 */
	public ErrorDTO() {
		super();
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * Gets the http status code.
	 *
	 * @return the http status code
	 */
	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	/**
	 * Sets the http status code.
	 *
	 * @param httpStatusCode
	 *            the new http status code
	 */
	public void setHttpStatusCode(final int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
}
