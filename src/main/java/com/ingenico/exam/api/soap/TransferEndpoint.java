package com.ingenico.exam.api.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ingenico.exam.api.soap.schemas.TransferRequest;
import com.ingenico.exam.api.soap.schemas.TransferResponse;
import com.ingenico.exam.api.soap.schemas.TransferType;
import com.ingenico.exam.constants.Constants;
import com.ingenico.exam.entity.Transfer;
import com.ingenico.exam.service.TransferService;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferEndpoint.
 */
@Endpoint
public class TransferEndpoint {

	/** The transfer service. */
	@Autowired
	private TransferService transferService;

	/**
	 * Transfer.
	 *
	 * @param transferRequest
	 *            the transfer request
	 * @return the transfer response
	 * @throws Exception
	 *             the exception
	 */
	@PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = "TransferRequest")
	@ResponsePayload
	public TransferResponse transfer(@RequestPayload final TransferRequest transferRequest) throws Exception {

		final TransferType transferType = transferRequest.getTransfer();
		transferService.save(new Transfer(transferType.getFromAccountId().longValue(),
				transferType.getToAccountId().longValue(), transferType.getAmount()));
		final TransferResponse transferResponse = new TransferResponse();
		transferResponse.setCode(HttpStatus.CREATED.toString());
		return transferResponse;
	}

}
