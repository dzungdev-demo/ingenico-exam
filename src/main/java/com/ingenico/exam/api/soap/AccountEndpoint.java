package com.ingenico.exam.api.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ingenico.exam.api.soap.schemas.AccountRequest;
import com.ingenico.exam.api.soap.schemas.AccountResponse;
import com.ingenico.exam.api.soap.schemas.AccountType;
import com.ingenico.exam.constants.Constants;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.service.AccountService;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class AccountEndpoint.
 */
@Endpoint
public class AccountEndpoint {

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/**
	 * Creates the account.
	 *
	 * @param accountRequest
	 *            the account request
	 * @return the account response
	 * @throws Exception
	 *             the exception
	 */
	@PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = "AccountRequest")
	@ResponsePayload
	public AccountResponse createAccount(@RequestPayload final AccountRequest accountRequest) throws Exception {

		final AccountType accountType = accountRequest.getAccount();
		accountService.save(new Account(accountType.getName(), accountType.getBalance()));
		final AccountResponse accountResponse = new AccountResponse();
		accountResponse.setCode(HttpStatus.CREATED.toString());
		return accountResponse;
	}

}
