package com.ingenico.exam.repository;

import com.ingenico.exam.entity.Transfer;

/**
 * @author DUNG TAN DANG
 * 
 *         The Interface TransferRepository.
 */
public interface TransferRepository extends IRepository<Transfer> {

}
