package com.ingenico.exam.exception;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class OverDrawnException.
 */
public class OverDrawnException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5134780144065566464L;

	/**
	 * Instantiates a new over drawn exception.
	 */
	public OverDrawnException() {
	}

	/**
	 * Instantiates a new over drawn exception.
	 *
	 * @param message
	 *            the message
	 */
	public OverDrawnException(final String message) {
		super(message);
	}

}
