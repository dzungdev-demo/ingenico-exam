package com.ingenico.exam.exception;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class DuplicatedException.
 */
public class DuplicatedException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7032507001813734469L;

	/**
	 * Instantiates a new duplicated exception.
	 */
	public DuplicatedException() {
	}

	/**
	 * Instantiates a new duplicated exception.
	 *
	 * @param message
	 *            the message
	 */
	public DuplicatedException(final String message) {
		super(message);
	}

}
