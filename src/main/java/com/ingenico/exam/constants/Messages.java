package com.ingenico.exam.constants;

/**
 * @author DUNG TAN DANG
 * 
 *         The Interface Messages.
 */
public interface Messages {

	/** The duplicated create. */
	String DUPLICATED_CREATE = "duplicated.create";

	/** The transfer amount negative. */
	String TRANSFER_AMOUNT_NEGATIVE = "transfer.amount.negative";

	/** The transfer over drawn. */
	String TRANSFER_OVER_DRAWN = "transfer.over.drawn";

	/** The account balance negative. */
	String ACCOUNT_BALANCE_NEGATIVE = "account.balance.negative";

	/** The not found exception. */
	String NOT_FOUND_EXCEPTION = "not.found.exception";

}
