package com.ingenico.exam.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingenico.exam.constants.Messages;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.exception.DuplicatedException;
import com.ingenico.exam.exception.NotFoundException;
import com.ingenico.exam.exception.NotPositiveAmountException;
import com.ingenico.exam.repository.AccountRepository;
import com.ingenico.exam.repository.IRepository;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class AccountService.
 */
@Service
@Transactional
public class AccountService extends AbstractService<Account> {

	/** The account repository. */
	@Autowired
	private AccountRepository accountRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ingenico.exam.service.AbstractService#getRepository()
	 */
	@Override
	public IRepository<Account> getRepository() {
		return accountRepository;
	}

	/**
	 * Save.
	 *
	 * @param account
	 *            the account
	 * @return the account
	 * @throws DuplicatedException
	 *             the duplicated exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	public Account save(final Account account) throws DuplicatedException, NotPositiveAmountException {
		if (account.getBalance() < 0) {
			throw new NotPositiveAmountException(msg(Messages.ACCOUNT_BALANCE_NEGATIVE));
		}
		final Account eixistedAccount = accountRepository.findByName(account.getName());
		if (eixistedAccount != null) {
			throw new DuplicatedException(msg(Messages.DUPLICATED_CREATE));
		}

		getRepository().save(account);
		return account;
	}

	/**
	 * Update.
	 *
	 * @param account
	 *            the account
	 * @return the account
	 * @throws NotFoundException
	 *             the not found exception
	 * @throws DuplicatedException
	 *             the duplicated exception
	 */
	public Account update(final Account account) throws NotFoundException, DuplicatedException {
		final Account existingEntity = getRepository().findOne(account.getId());
		if (existingEntity == null) {
			throw new NotFoundException(msg(Messages.NOT_FOUND_EXCEPTION));
		}
		if (!account.getName().equals(existingEntity.getName())
				&& accountRepository.findByName(account.getName()) != null) {
			throw new DuplicatedException();
		}

		existingEntity.setName(account.getName());
		existingEntity.setBalance(account.getBalance());

		getRepository().save(existingEntity);
		return existingEntity;
	}

	/**
	 * Find by name.
	 *
	 * @param name
	 *            the name
	 * @return the account
	 * @throws NotFoundException
	 *             the not found exception
	 */
	public Account findByName(final String name) throws NotFoundException {
		final Account existingEntity = accountRepository.findByName(name);
		if (existingEntity == null) {
			throw new NotFoundException(msg(Messages.NOT_FOUND_EXCEPTION));
		}
		return existingEntity;
	}

}
