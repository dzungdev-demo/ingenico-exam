package com.ingenico.exam.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingenico.exam.constants.Messages;
import com.ingenico.exam.entity.Account;
import com.ingenico.exam.entity.Transfer;
import com.ingenico.exam.exception.NotPositiveAmountException;
import com.ingenico.exam.exception.OverDrawnException;
import com.ingenico.exam.repository.AccountRepository;
import com.ingenico.exam.repository.IRepository;
import com.ingenico.exam.repository.TransferRepository;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferService.
 */
@Service
@Transactional
public class TransferService extends AbstractService<Transfer> {

	/** The transfer repository. */
	@Autowired
	private TransferRepository transferRepository;

	/** The account repository. */
	@Autowired
	private AccountRepository accountRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ingenico.exam.service.AbstractService#getRepository()
	 */
	@Override
	public IRepository<Transfer> getRepository() {
		return transferRepository;
	}

	/**
	 * Save.
	 *
	 * @param transfer
	 *            the transfer
	 * @throws OverDrawnException
	 *             the over drawn exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	public void save(final Transfer transfer) throws OverDrawnException, NotPositiveAmountException {
		if (transfer.getAmount() <= 0) {
			throw new NotPositiveAmountException(msg(Messages.TRANSFER_AMOUNT_NEGATIVE));
		}
		final Account fromAccount = accountRepository.findById(transfer.getFromAccountId());
		final Account toAccount = accountRepository.findById(transfer.getToAccountId());
		if (fromAccount.getBalance() < transfer.getAmount()) {
			throw new OverDrawnException(msg(Messages.TRANSFER_OVER_DRAWN));
		}
		fromAccount.setBalance(fromAccount.getBalance() - transfer.getAmount());
		toAccount.setBalance(toAccount.getBalance() + transfer.getAmount());
		transfer.setFromAccount(fromAccount);
		transfer.setToAccount(toAccount);

		transferRepository.save(transfer);
	}

}
