package com.ingenico.exam;

import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class SoapConfig.
 */
@EnableWs
@Configuration
public class SoapConfig extends WsConfigurerAdapter {

	/**
	 * Message dispatcher servlet.
	 *
	 * @param applicationContext
	 *            the application context
	 * @return the servlet registration bean
	 */
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(final ApplicationContext applicationContext) {
		final MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/ws/v1/*");
	}

	/**
	 * Payment schema.
	 *
	 * @return the xsd schema
	 */
	@Bean
	public XsdSchema paymentSchema() {
		return new SimpleXsdSchema(new ClassPathResource("META-INF/schemas/payment.xsd"));
	}

	/**
	 * Default wsdl 11 definition.
	 *
	 * @param paymentSchema
	 *            the payment schema
	 * @return the default wsdl 11 definition
	 */
	@Bean(name = "payment")
	public DefaultWsdl11Definition defaultWsdl11Definition(final XsdSchema paymentSchema) {
		final DefaultWsdl11Definition wsdl = new DefaultWsdl11Definition();
		wsdl.setPortTypeName("Payment");
		wsdl.setLocationUri("/ws/v1");
		wsdl.setTargetNamespace("http://ingenico.com/payment/definitions");
		wsdl.setSchema(paymentSchema);
		return wsdl;
	}

}
